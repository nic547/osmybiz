#
# This file is autogenerated by pip-compile with Python 3.11
# by the following command:
#
#    pip-compile requirements.in
#
alembic==1.4.2
    # via flask-migrate
atomicwrites==1.4.1
    # via pytest
attrs==19.3.0
    # via pytest
blinker==1.4
    # via raven
click==7.1.1
    # via flask
colorama==0.4.6
    # via pytest
entrypoints==0.3
    # via flake8
flake8==3.7.9
    # via pytest-flake8
flask==1.1.1
    # via
    #   -r requirements.in
    #   flask-migrate
    #   flask-script
    #   flask-sqlalchemy
    #   pytest-flask
    #   raven
flask-migrate==2.5.3
    # via -r requirements.in
flask-script==2.0.6
    # via -r requirements.in
flask-sqlalchemy==2.4.1
    # via
    #   -r requirements.in
    #   flask-migrate
itsdangerous==1.1.0
    # via flask
jinja2==2.11.1
    # via flask
mako==1.1.2
    # via alembic
markupsafe==1.1.1
    # via
    #   jinja2
    #   mako
mccabe==0.6.1
    # via flake8
more-itertools==8.2.0
    # via pytest
packaging==20.3
    # via pytest
pluggy==0.13.1
    # via pytest
psycopg2-binary==2.8.4
    # via -r requirements.in
py==1.8.1
    # via pytest
pycodestyle==2.5.0
    # via flake8
pyflakes==2.1.1
    # via flake8
pyparsing==2.4.6
    # via packaging
pytest==5.4.1
    # via
    #   pytest-flake8
    #   pytest-flask
pytest-flake8==1.0.4
    # via -r requirements.in
pytest-flask==1.0.0
    # via -r requirements.in
python-dateutil==2.8.1
    # via alembic
python-editor==1.0.4
    # via alembic
raven[flask]==6.10.0
    # via -r requirements.in
six==1.14.0
    # via
    #   packaging
    #   python-dateutil
sqlalchemy==1.3.15
    # via
    #   alembic
    #   flask-sqlalchemy
wcwidth==0.1.8
    # via pytest
werkzeug==1.0.0
    # via
    #   flask
    #   pytest-flask
