Tools
=====

Navigate to `/src` and run `pyhton build_presets.py lang`, where lang is the language key you want to build the presets for.

To add another language copy the corresponding .json file from the id editor tagging scheme ([link](https://github.com/openstreetmap/id-tagging-schema/tree/main/dist/translations)) into the locales directory.
