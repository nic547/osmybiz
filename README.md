# OpenStreetMap My Business 

Homepage https://osmybiz.osm.ch/ 

## Testing

Test-Instance: https://osmybiz-staging.geoh.infs.ch/

Note: Existing businesses are loaded from Overpass and do not exist in the development database of OSM.
Saving them will fail. Businesses created in OSMyBiz can be edited, as they get created on the development database.

## local development

Prerequisites: 

* Account at https://master.apis.dev.openstreetmap.org/
* Docker and docker-compose installed and running locally

0. At https://master.apis.dev.openstreetmap.org/ register a new application under OAuth 2 applications,
    i.e. `https://master.apis.dev.openstreetmap.org/oauth2/applications/new`.
    Add the Redirect URIs as in the example: `https://localhost:8089/land`
    Select the correct options, `read prefrences`, `modify map`, and `modify notes`.

    ![](./documentation_images/osm-auth-selection.png)

1. Copy the file .env.example to .env and fill out the fields with the client id and client secret you just created.

2. Run the whole application locally using `docker-compose up --build`. 

3. Visit https://localhost:8089 and accept the certificate (make an exception).


### Adding new language

1. Add the new language file from https://www.transifex.com/openstreetmap/osmybiz/
into ./frontend/osmybiz/src/locales

2. Modify a copy of another language file found under ./frontend/osmybiz/src/assets/tags/
using the translation found here https://github.com/openstreetmap/id-tagging-schema/tree/main/dist/translations

3. In ./frontend/osmybiz/src/stores/locale.ts, import the json from ./tags/ and add it to SUPPORTEDLANGUAGESOPTIONS. Keep both in alphabetical order.

4. In ./frontent/osmybiz/src/locales/i18n-config.ts, import the json for the locale and add it in the message list.

5. Lastly, add the language in ./components/shared/LanguageDropDown.vue to SUPPORTEDLANGUAGESOPTIONS.

### Contributors

Dominic Ritz, André Blöchlinger, Khoa Tran, David Kalchofner
