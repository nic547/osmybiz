import { query } from "../api/nominatimApi.js";
import { saveCoordsZoomIntoLocalStorage } from "../util/positionUtil";

import { initialPosition, initialZoom } from "@/config/config.js";
import { ref, type Ref } from "vue";
import { defineStore } from "pinia";
import type { LeafletMap, Suggestion } from "@/types/types.js";
import { LatLng, latLng } from "leaflet";

const defaultMap = {
  center: initialPosition,
  zoom: initialZoom,
  minZoom: 3,
  maxZoom: 30,
  maxBounds: [],
  bounds: [],
  leafletObject: {},
};

const defaultViewPort = {
  boundingBox: {
    south: 0,
    east: 0,
    west: 0,
    north: 0,
  },
  zoom: 18,
};

type ViewPort = {
  boundingBox: {
    south: number;
    east: number;
    west: number;
    north: number;
  };
  zoom: number;
};

// map and landing view store / information and search/search suggestions
export const useLandingStore = defineStore("landing", () => {
  const map: Ref<LeafletMap> = ref(defaultMap);
  const bounds = ref({});
  const mapCenter: Ref<LatLng> = ref(initialPosition);
  const mapZoom = ref(initialZoom);
  const urlParams = ref(null); // to be typed
  const viewPort: Ref<ViewPort> = ref(defaultViewPort);
  const mode = ref("vector");
  const showHelp = ref(true);
  const showLoginHelp = ref(true);
  const applyOffset = ref(true);
  const openPopUpForId: Ref<{ id: number; coords: object }> = ref({
    id: 0,
    coords: {},
  });

  // search
  const suggestions: Ref<Array<Suggestion>> = ref([]);
  const search = ref("");

  /* decide if we want to keep and use this or remove completly... 
  function setMapViewToUrl(params: { zoom: string; lat: string; lng: string }) {
    // actually don't need this whole thing anymore -> done via mapCenter and mapZoom
    // todo figure out if we need this getPositionFromURL
    // const pos = getPositionFromUrl(params);
  } */
  function setMapViewToCoordsZoom(
    coords: { lat: number; lng: number },
    zoom: number
  ) {
    // don't need this anymore either tbh
    map.value.center = latLng(coords.lat, coords.lng);
    map.value.zoom = zoom;
  }

  function setLastKnownPosition(
    coords: { lat: number; lng: number },
    zoom: number
  ) {
    saveCoordsZoomIntoLocalStorage(coords, zoom);
  }

  // search suggestions via searchbar
  async function queryNominatim(search: string, language: string) {
    suggestions.value = await query(search, language);
  }

  function resetSearch() {
    search.value = "";
    suggestions.value = [];
  }

  return {
    map,
    mapCenter,
    mapZoom,
    urlParams,
    search,
    viewPort,
    mode,
    showHelp,
    showLoginHelp,
    applyOffset,
    suggestions,
    bounds,
    openPopUpForId,
    setMapViewToCoordsZoom,
    setLastKnownPosition,
    queryNominatim,
    resetSearch,
  };
});
