import tagsAr from "../assets/tags/ar.json";
import tagsCa from "../assets/tags/ca.json";
import tagsCs from "../assets/tags/cs.json";
import tagsEn from "../assets/tags/en.json";
import tagsEs from "../assets/tags/es.json";
import tagsDe from "../assets/tags/de.json";
import tagsFa from "../assets/tags/fa.json";
import tagsFr from "../assets/tags/fr.json";
import tagsHe from "../assets/tags/he.json";
import tagsHu from "../assets/tags/hu.json";
import tagsIs from "../assets/tags/is.json";
import tagsIt from "../assets/tags/it.json";
import tagsNl from "../assets/tags/nl.json";
import tagsPl from "../assets/tags/pl.json";
import tagsRu from "../assets/tags/ru.json";
import tagsSv from "../assets/tags/sv.json";
import tagsZh_TW from "../assets/tags/zh-TW.json";

export const FALLBACKLOCALE = "en";

const SUPPORTEDLANGUAGESOPTIONS: any = {
  ar: tagsAr,
  ca: tagsCa,
  cs: tagsCs,
  de: tagsDe,
  en: tagsEn,
  es: tagsEs,
  fa: tagsFa,
  fr: tagsFr,
  he: tagsHe,
  hu: tagsHu,
  is: tagsIs,
  it: tagsIt,
  nl: tagsNl,
  pl: tagsPl,
  ru: tagsRu,
  sv: tagsSv,
  zh_TW: tagsZh_TW,
};

const FALLBACKTAGS = SUPPORTEDLANGUAGESOPTIONS[FALLBACKLOCALE];

import { ref, type Ref } from "vue";
import { defineStore } from "pinia";

type LanguageTag = {
  [key: string]: any;
  name: any;
  fields: Array<Field>;
};

type Field = {
  key: string;
  label: string;
  type: string;
  value?: string;
  options?: any;
};

// maybe rename to make clearer
type Option = {
  value: string;
  text: string;
  fields: Array<Field>;
};

export const useLocaleStore = defineStore("locale", () => {
  const languageTags: Ref<Array<LanguageTag>> = ref([]);
  const categoryFields: Ref<Array<Option>> = ref([]);

  function setTags(language: string) {
    const tags = SUPPORTEDLANGUAGESOPTIONS[language];
    Object.keys(FALLBACKTAGS).forEach((key: any) => {
      if (tags[key]) {
        languageTags.value[key] = tags[key];
      } else {
        languageTags.value[key] = FALLBACKTAGS[key];
      }
    });
    const options = getCategoryOptions(languageTags.value);
    options.sort((a, b) => {
      if (a.text < b.text) return -1;
      if (a.text > b.text) return 1;
      return 0;
    });
    categoryFields.value = options;
  }

  function getTagName(tag: any): { name: string; fields: Array<Field> } {
    return languageTags.value[tag] || tag;
  }

  return { languageTags, categoryFields, setTags, getTagName };
});

function getFieldOptions(field: Field) {
  const fieldOptions: Array<any> = [];
  Object.keys(field.options).forEach((option) => {
    fieldOptions.push({
      key: option,
      text: field.options[option],
    });
  });
  return fieldOptions;
}

function getFields(category: any) {
  const fields: Array<any> = [];
  category.fields.forEach((field: Field) => {
    const content: Field = {
      value: "",
      options: [],
      ...field,
    };
    if (field.options) {
      content.options = getFieldOptions(field);
    }
    fields.push(content);
  });
  return fields;
}

function getCategoryOptions(languageTags: Array<LanguageTag>): Array<Option> {
  const options: Array<Option> = [];
  Object.keys(languageTags).forEach((key: any) => {
    options.push({
      value: key,
      text: languageTags[key].name,
      fields: getFields(languageTags[key]),
    });
  });
  return options;
}
