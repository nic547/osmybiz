import { defineStore } from "pinia";
import { ref } from "vue";
import type { Ref } from "vue";
import type { OverpassElement, Bbox, CurrentBPOI } from "@/types/types";
import { queryBox } from "@/api/overpassApi";

const minZoomForBusinesses = 17;
// overpass elements and bpois

export const useBPOIStore = defineStore("bpoi", () => {
  const currentBPOI = ref({});
  const businessPOIs: Ref<Array<CurrentBPOI>> = ref([]); // elements that are loaded from nominatim / our db
  const overpassElements: Ref<Array<OverpassElement>> = ref([]); // elements that are loaded from overpass

  async function queryOverpass(boundingBox: Bbox, zoom: number) {
    if (zoom < minZoomForBusinesses) {
      overpassElements.value = [];
      return;
    }
    const res = await queryBox(boundingBox);
    overpassElements.value = res;
    /*res.forEach((element) => { until efficient way found, can't do this, as it is too slow...
      // like this we have unique values and don't overwrite elements
      overpassElements.value[element.id] = element;
    });*/
  }

  async function getBPOIIfStored(osmId: number) {
    return businessPOIs.value[osmId] || {};
  }

  async function storeBPOI(bpoi: any) {
    businessPOIs.value[bpoi.osmId] = bpoi;
  }

  return {
    currentBPOI,
    overpassElements,
    businessPOIs,
    queryOverpass,
    getBPOIIfStored,
    storeBPOI,
  };
});
