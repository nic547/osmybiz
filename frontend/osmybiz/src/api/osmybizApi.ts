import axios from "axios";
import { fakeOsmybizApi, osmyBizBackendUrl } from "../config/config.js";
import {
  mockAddOrUpdateUser,
  mockAddOrUpdateBusinessPOI,
  mockFetchBusinessPOIs,
  mockUnsubscribe,
} from "./osmybizApiMock";
import { useErrorStore } from "@/stores/error.js";
import type { BPOI } from "@/types/types.js";

const baseRoute = osmyBizBackendUrl;

if (fakeOsmybizApi) {
  console.warn("Using fake osmybiz backend");
}

export async function addOrUpdateUser(
  userId: number,
  displayName: string
): Promise<void> {
  if (fakeOsmybizApi) {
    return mockAddOrUpdateUser(userId, displayName);
  }
  const route = `${baseRoute}user`;
  try {
    return await axios.post(route, { osmId: userId, username: displayName });
  } catch (e) {
    // currently don't throw the exception as not relevant for user
  }
}

export async function fetchBusinessPOIs(userId: number): Promise<any> {
  if (fakeOsmybizApi) {
    return mockFetchBusinessPOIs(userId);
  }
  const route = `${baseRoute}user/${userId}/business-poi`;
  try {
    const response = await axios.get(route);
    return response.data;
  } catch {
    const errorStore = useErrorStore();

    errorStore.setError({ errorMessageKey: "error.osmybiz" });
  }
}

/**
 * Add or updates a POI to the watchlist database. Does not update the local watchlist.
 * @param userId The ID of the user
 * @param businessPOI The business POI to add or update
 * @returns true if the operation was successful, false otherwise
 */
export async function addOrUpdateBusinessPOI(
  userId: number,
  businessPOI: BPOI
): Promise<boolean> {
  if (fakeOsmybizApi) {
    return mockAddOrUpdateBusinessPOI(userId, businessPOI);
  }
  const route = `${baseRoute}user/${userId}/business-poi`;
  try {
    await axios.post(route, businessPOI);
    return true;
  } catch {
    const errorStore = useErrorStore();

    errorStore.setError({ errorMessageKey: "error.osmybiz" });
    return false;
  }
}

export async function getTemporaryOsmId(
  userId: string
): Promise<number | null> {
  // NOT IMPLEMENTED for fakeOsmmybiz
  // if (fakeOsmybizApi) {
  //    return mockAddOrUpdateNote(userId);
  // }
  const route = `${baseRoute}user/${userId}/temporary-osm-id`;
  try {
    const response = await axios.get(route);
    return response.data;
  } catch {
    const errorStore = useErrorStore();

    errorStore.setError({ errorMessageKey: "error.osmybiz" });
  }
  return null;
}

export async function unsubscribe(
  userId: number,
  osmId: number
): Promise<void> {
  if (fakeOsmybizApi) {
    return mockUnsubscribe(userId, osmId);
  }
  const route = `${baseRoute}user/${userId}/business-poi/${osmId}/unsubscribe`;
  try {
    return await axios.post(route);
  } catch {
    const errorStore = useErrorStore();

    errorStore.setError({ errorMessageKey: "error.osmybiz" });
  }
}
