import { latLng } from "leaflet";
import { initialPosition, initialZoom } from "../config/config";

export const FALLBACKPOSITION = { coords: initialPosition, zoom: initialZoom };
const position = "last_known_position";

type Coords = { lat: number; lng: number };

export function getCoordsZoomFromLocalStorage():
  | { coords: Coords; zoom: number }
  | undefined {
  const stored: { coords: Coords; zoom: number } = JSON.parse(
    localStorage.getItem(position) || "{}"
  );

  // ensure that we actually loaded a set of cords and zoom, rather than an empty object
  if( stored.coords && stored.coords.lat && stored.coords.lng && stored.zoom ) {
    return stored;
  }
  return undefined;
}

export function saveCoordsZoomIntoLocalStorage(coords: Coords, zoom: number) {
  const pos: { coords: Coords; zoom: number } = {
    coords,
    zoom,
  };
  localStorage.setItem(position, JSON.stringify(pos));
}

function isNumberLatLngZoom(latLngZoom: {
  lat: number;
  lng: number;
  zoom: number;
}) {
  let { lat, lng, zoom } = latLngZoom;
  [lat, lng, zoom] = [Number(lat), Number(lng), Number(zoom)];
  return !Number.isNaN(lat) && !Number.isNaN(lng) && !Number.isNaN(zoom);
}

function isValidPositionParams(params?: { coords: Coords; zoom: number }) {
  if (!params) {
    return false;
  }
  const { coords, zoom } = params;
  const { lat, lng } = coords;
  return isNumberLatLngZoom({ lat, lng, zoom });
}

function latLngZoomToCoordsZoom(latLngZoom: {
  lat: number;
  lng: number;
  zoom: number;
}): { coords: Coords; zoom: number } | undefined {
  if (isNumberLatLngZoom(latLngZoom)) {
    const coords = latLng(latLngZoom.lat, latLngZoom.lng);
    const { zoom } = latLngZoom;
    return { coords, zoom };
  }
  return undefined;
}

function getCoordsZoomFromUrl(params: {
  lat: string;
  lng: string;
  zoom: string;
}) {
  const latLngZoom: { lat: number; lng: number; zoom: number } = {
    lat: Number(params.lat),
    lng: Number(params.lng),
    zoom: Number(params.zoom),
  };
  const coordsZoom = latLngZoomToCoordsZoom(latLngZoom);
  return isValidPositionParams(coordsZoom) ? coordsZoom : undefined;
}

export function getPositionFromUrl(params: {
  lat: string;
  lng: string;
  zoom: string;
}) {
  const posFromUrl = getCoordsZoomFromUrl(params);
  if (isValidPositionParams(posFromUrl)) {
    return posFromUrl;
  }
  const posFromLocalStorage = getCoordsZoomFromLocalStorage();
  if (isValidPositionParams(posFromLocalStorage)) {
    return posFromLocalStorage;
  }
  return FALLBACKPOSITION;
}
