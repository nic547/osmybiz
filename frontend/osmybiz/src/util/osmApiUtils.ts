import { xml2json } from "xml-js";
import { osmUrl } from "../config/config.js";
import type { BusinessPOI, POI } from "@/types/types";

const detailTags = [
  "name",
  "opening_hours",
  "opening_hours:url",
  "check_date:opening_hours",
  "check_date:opening_hours:url",
  "phone",
  "email",
  "website",
  "wheelchair",
  "description",
  "note",
];

const addressTags = [
  {
    k: "addr:street",
    v: "street",
  },
  {
    k: "addr:housenumber",
    v: "housenumber",
  },
  {
    k: "addr:place",
    v: "place",
  },
  {
    k: "addr:postcode",
    v: "postcode",
  },
  {
    k: "addr:city",
    v: "city",
  },
];

const otherKnownTags = ["amenity", "leisure", "shop", "office", "tourism"];

const osmChangeVersion = "0.6";

const knownTags = addressTags
  .map((tag) => tag.k)
  .concat(detailTags)
  .concat(otherKnownTags);

function parseDetails(businessPOITags: any) {
  const details: any = {};

  Object.keys(businessPOITags).forEach((key) => {
    detailTags.forEach((tag) => {
      if (tag === key) {
        details[tag] = businessPOITags[key];
      }
    });
  });

  return details;
}

function parseAddress(businessPOITags: any) {
  const address: any = {};

  Object.keys(businessPOITags).forEach((key) => {
    addressTags.forEach((tag) => {
      if (tag.k === key) {
        address[tag.v] = businessPOITags[key];
      }
    });
  });

  return address;
}

/**
 @description Parses all tags that are not known as "base tags"
*/
function parseUnknownTags(tags: any): any {
  const unknownTags: any = {};

  for (const key in tags) {
    if (!knownTags.includes(key)) {
      unknownTags[key] = tags[key];
    }
  }

  return unknownTags;
}

function getAttributes(obj: { _attributes: any }) {
  return obj._attributes;
}

type User = {
  name: string;
  id: number;
  unReadCount?: number;
  langPrefs?: Array<any>;
};

function parseUser(userResponse: string): User | {} {
  const { user } = JSON.parse(userResponse);
  return user
    ? {
        name: user.display_name,
        id: user.id,
        unReadCount: user.messages.received.unread,
        langPrefs: user.languages,
      }
    : {};
}

function createAddressTags(businessPOI: BusinessPOI) {
  let text = "";
  if (businessPOI.address.street) {
    text += `<tag k="addr:street" v="${businessPOI.address.street}"/>`;
    if (businessPOI.address.housenumber) {
      text += `<tag k="addr:housenumber" v="${businessPOI.address.housenumber}"/>`;
    }
  }
  if (businessPOI.address.place) {
    text += `<tag k="addr:place" v="${businessPOI.address.place}"/>`;
  }
  if (businessPOI.address.postcode) {
    text += `<tag k="addr:postcode" v="${businessPOI.address.postcode}"/>`;
  }
  if (businessPOI.address.city) {
    text += `<tag k="addr:city" v="${businessPOI.address.city}"/>`;
  }
  return text;
}

function createDetailTags(businessPOI: BusinessPOI) {
  let text = "";
  if (businessPOI.details.opening_hours.length !== 0) {
    text += `<tag k="opening_hours" v="${businessPOI.details.opening_hours}"/>`;
    text += `<tag k="check_date:opening_hours" v="${getCurrentDateForCheckDateTag()}"/>`;
  }
  // TODO: Find out why `opening_url` has to be treated differently
  //       than the other properties of `businessPOI.details`.
  //       https://gitlab.com/geometalab/osmybiz/issues/240
  if (businessPOI.details.opening_url) {
    // if opening_url is neither undefined nor empty
    text += `<tag k="opening_hours:url" v="${businessPOI.details.opening_url}"/>`;
    text += `<tag k="check_date:opening_hours:url" v="${getCurrentDateForCheckDateTag()}"/>`;
  }
  if (businessPOI.details.phone.length !== 0) {
    text += `<tag k="phone" v="${businessPOI.details.phone}"/>`;
  }
  if (businessPOI.details.email.length !== 0) {
    text += `<tag k="email" v="${businessPOI.details.email}"/>`;
  }
  if (businessPOI.details.website.length !== 0) {
    text += `<tag k="website" v="${businessPOI.details.website}"/>`;
  }
  if (businessPOI.details.wheelchair.length !== 0) {
    text += `<tag k="wheelchair" v="${businessPOI.details.wheelchair}"/>`;
  }
  if (businessPOI.details.description.length !== 0) {
    text += `<tag k="description" v="${businessPOI.details.description}"/>`;
  }
  if (businessPOI.details.note.length !== 0) {
    text += `<tag k="note" v="${businessPOI.details.note}"/>`;
  }

  if (businessPOI.details.category.fields) {
    businessPOI.details.category.fields.forEach((field: any) => {
      if (field.value.length !== 0) {
        text += `<tag k="${field.key}" v="${field.value}"/>`;
      }
    });
  }
  return text;
}

function getCurrentDateForCheckDateTag() {
  const today = new Date();
  const year = today.getFullYear();
  const month = today.getMonth() + 1;
  const date = today.getDate();
  return year + "-" + ((month > 9 ? '' : '0') + month) + "-" + ((date > 9 ? '' : '0') + date);
}

function createUnknownTagsXml(businessPOI: any) {
  let text = "";
  const presetTags = businessPOI.details.category.fields.map(
    (field: any) => field.key
  );
  for (const key in businessPOI.unknownTags) {
    // Some "unknown" tags are simply tags from presets, but we can't tell that when parsing them, so we have to check here.
    if (!presetTags.includes(key)) {
      text += `<tag k="${key}" v="${businessPOI.unknownTags[key]}"/>`;
    }
  }
  return text;
}

function constructUpload(businessPOI: BusinessPOI, changeSetId: string) {
  const operation = businessPOI.osmId < 0 ? "create" : "modify";

  let xml =
    "" +
    `<osmChange version="${osmChangeVersion}" generator="OSMyBiz">` +
    `<${operation}>` +
    `<${businessPOI.osmType} id="${businessPOI.osmId}" ${
      businessPOI.version !== 0 ? `version="${businessPOI.version}"` : ``
    }` +
    ` lat="${businessPOI.lat}"` +
    ` lon="${businessPOI.lon}"` +
    ` changeset="${changeSetId}">` +
    `<tag k="name" v="${businessPOI.details.name}"/>`;

  if (businessPOI.details.category.value !== 0) {
    const category = businessPOI.details.category.value.split("/");
    xml += `<tag k="${category[0]}" v="${category[1]}"/>`;
  }

  xml += createAddressTags(businessPOI);

  xml += createDetailTags(businessPOI);

  xml += createUnknownTagsXml(businessPOI);

  xml += "</node>" + `</${operation}>` + "</osmChange>";

  return xml;
}

function parseTags(businessPOIJson: { tag: Array<object> }) {
  const tags = businessPOIJson.tag;
  const result: any = {};
  tags.forEach((t: any) => {
    const tagAttributes = getAttributes(t);
    result[tagAttributes.k] = tagAttributes.v;
  });
  return result;
}

function parseNoteStatus(noteXml: any) {
  return JSON.parse(xml2json(noteXml.data, { compact: true })).osm.note.status
    ._text;
}

function parseBusinessPOI(businessPOIXml: any, osmType: string): POI {
  let businessPOIJson;
  if (typeof businessPOIXml === "string") {
    businessPOIJson = JSON.parse(xml2json(businessPOIXml, { compact: true }))
      .osm[osmType];
  } else {
    const xml = businessPOIXml.getElementsByTagName("osm")[0].innerHTML;
    businessPOIJson = JSON.parse(xml2json(xml, { compact: true })).node;
  }

  const businessPOIAttributes = getAttributes(businessPOIJson as any);

  const tags = parseTags(businessPOIJson as any);

  const address = parseAddress(tags);
  const details = parseDetails(tags);
  const unknownTags = parseUnknownTags(tags);

  return {
    id: parseInt(businessPOIAttributes.id, 10),
    lat: businessPOIAttributes.lat,
    lon: businessPOIAttributes.lon,
    link: `${osmUrl}/node/${businessPOIAttributes.id}/#map=19/${businessPOIAttributes.lat}/${businessPOIAttributes.lon}&layers=D`,
    address,
    details,
    unknownTags,
    version: businessPOIAttributes.version,
    changeSet: businessPOIAttributes.changeset,
    tags,
    noteId: "",
    noteIsResolved: false,
    hasUpdate: false,
  };
}

function extractId(businessPOIDiff: HTMLElement) {
  const xml = businessPOIDiff.getElementsByTagName("diffResult")[0].innerHTML;
  const diffJson = JSON.parse(xml2json(xml, { compact: true })).node;

  return getAttributes(diffJson).new_id;
}

export default {
  parseUser,
  constructUpload,
  parseBusinessPOI,
  extractId,
  parseNoteStatus,
};
