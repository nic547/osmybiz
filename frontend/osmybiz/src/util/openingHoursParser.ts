import {checkOpeningHoursNotNullOrUndefined} from "./converter";
import {createEmptyDetails} from "../stores/detail";
import {opening_hours_days} from "@/config/config";
import type { OH_Details } from "@/types/types";

function isOpen(input: string): boolean {
  const emptyDetails = createEmptyDetails();
  for (let i = 0; i < emptyDetails.opening_hours_template!.length; i++) {
    if (input.includes(emptyDetails.opening_hours_template![i].day)) {
      return true;
    }
  }
  return false;
}

export function parseOpeningHoursString(input: any): OH_Details {
  const parsedOpeningHoursTemplate: OH_Details = createEmptyDetails().opening_hours_template!;
  if (input === undefined || input === "") {
    return parsedOpeningHoursTemplate;
  }
  if (!isValidOpeningHours(input)) {
    return parsedOpeningHoursTemplate;
  }
  if (isAlwaysOpen(input)) {
    parsedOpeningHoursTemplate![8].isOpen = true;
  } else if (isComplexOpeningHours(input)) {
    return parsedOpeningHoursTemplate;
  } else if (checkOpeningHoursNotNullOrUndefined(input)) {
      const daysAndTimesPart = input.split(";");
      for (let i = 0; i < daysAndTimesPart.length; i++) {
        if (daysAndTimesPart[i] === undefined || checkIfMoreThanThreeTimesIncluded(daysAndTimesPart[i])) {
          return parsedOpeningHoursTemplate;
        }

        daysAndTimesPart[i] = daysAndTimesPart[i].trim();
        if (daysAndTimesPart[i].includes("off")) {
          continue;
        } else {
          if (isOpen(daysAndTimesPart[i])) {
            parsedOpeningHoursTemplate![i].isOpen = true;
          }
        }
        const timesPart = daysAndTimesPart[i].split(" ");
        let times: string = "";
        for (let j = 1; j < timesPart.length; j++) {
          times += timesPart[j];
        }
        times = times.replace(" ", "");
        const sameDays: Array<string> = checkConnectedDays(timesPart[0]);
        const {opensAt, closesAt} = checkTimes(times);
        connectDaysAndTimes(parsedOpeningHoursTemplate, sameDays, opensAt, closesAt);
      }
    } 
  return parsedOpeningHoursTemplate;
}

function checkIfMoreThanThreeTimesIncluded(input: string): boolean {
  const dayAndTimes = input.split(" ");
  let timesInput: string = dayAndTimes[1];
  timesInput = timesInput.trim();
  const times = timesInput.split(",");
  return times.length > 3;
}

function isValidOpeningHours(input: string): boolean {
  const allowedCharactersPattern = /^((\s{0,}((((((Mo|Tu|We|Th|Fr|Sa|Su)-(Mo|Tu|We|Th|Fr|Sa|Su)\s{1,})|((Mo|Tu|We|Th|Fr|Sa|Su|PH)\s)|((Mo|Tu|We|Th|Fr|Sa|Su|PH),(Mo|Tu|We|Th|Fr|Sa|Su|PH)\s{1,}))(((\d{1,2}:\d{2}-\d{1,2}:\d{2}){1})((,)\s{0,}\d{1,2}:\d{2}-\d{1,2}:\d{2}){0,})|((Mo|Tu|We|Th|Fr|Sa|Su)\s(off))))|(PH off))(;)?)|(24\/7)){1,}$/;
  const isAllowed: boolean = allowedCharactersPattern.test(input);
  return isAllowed;
}


function isComplexOpeningHours(input: string): boolean {
  // currently checks if there's no month information, special characters like brackets or string with more then 4 chars
  const complexOpeningHoursPattern = /(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec|[a-zA-Z]{4,})/;
  return complexOpeningHoursPattern.test(input);
}


function isAlwaysOpen(input: string): boolean {
  if (input.includes("24/7")) {
    return true;
  } else {
    return false;
  }
}

function checkConnectedDays(input: string): string[] {
  const result: Array<string> = [];
  const separate: Array<string> = input.split(" ");
  if (separate[0].includes("-")) {
    for (let i = 0; i < opening_hours_days.length; i++) {
      if (separate[0].includes(opening_hours_days[i].input)) {
        result.push(opening_hours_days[i].value);
      }
    }
    return result[0].split('/');
  } else if (separate[0].includes(",")) {
    return separate[0].split(",");
  } else {
    return separate;
  }
}

function checkTimes(input: string) {
  const result: Array<string> = input.split(",");
  // currently only 3 times per day are supported
  const separateTimes: Array<string> = ["", "", "", "", "", ""];
  const opensAt: Array<string> = [];
  const closesAt: Array<string> = [];
  for (let i = 0; i < result.length; i++) {
    const index = i * 2;
    const tmp: Array<string> = result[i].split("-");
      separateTimes[index] = tmp[0];
      separateTimes[index + 1] = tmp[1];
  }
  for (let j = 0; j < separateTimes.length; j++) {
    if (j % 2 === 0) {
      opensAt.push(separateTimes[j]);
    } else {
      closesAt.push(separateTimes[j]);
    }
  }
  return {opensAt, closesAt};
}

function connectDaysAndTimes(parsedOpeningHoursTemplate: OH_Details, days: string[], opensAt: string[], closesAt: string[]) {
  for (let j = 0; j < days.length; j++) {
    for (let i = 0; i < parsedOpeningHoursTemplate!.length; i++) {
      for (let k = 0; k < closesAt.length; k++) {
        if (parsedOpeningHoursTemplate![i].day.includes(days[j])) {
          parsedOpeningHoursTemplate![i].opensAt[k] = opensAt[k];
          parsedOpeningHoursTemplate![i].closesAt[k] = closesAt[k];
          if (k === 1) {
            parsedOpeningHoursTemplate![i].addTimes[0] = closesAt[k] !== "";
          }
          if (k === 2) {
            parsedOpeningHoursTemplate![i].addTimes[1] = closesAt[k] !== "";
          }
        }
      }
    }
  }
}
