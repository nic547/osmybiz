import { latLng } from "leaflet";

// accesstoken for satellite imagery of mapbox can be generated with mapbox account
export const mapBoxToken =
  "pk.eyJ1IjoibXRoaSIsImEiOiJjajlzZHJqZGc2bGRxMnhxbTd0bjVibDNjIn0.11MBq0_6S30JBIw7oo9O7A";

// export const initialPosition = latLng(46.87897, 8.23975) // aprox center of switzerland
// export const initialZoom = 8
export const initialPosition = latLng(46.87897, 8.23975);
export const initalPositionObj = { lat: 46.87897, lng: 8.23975 };
export const initialZoom = 11;

export const LatLngRoundingAccuracy = 5;
export const searchradius = 10.0;

// change between development (dev) and production (prod)
export const osmApiLevel = "/api/0.6/";
export const osmUrl = import.meta.env.VITE_OSM_URL;

// nominatim Urls
export const nominatimUrl = "https://nominatim.openstreetmap.org/search";
export const nominatimReverseUrl =
  "https://nominatim.openstreetmap.org/reverse";

// overpass api url
export const overpassUrl = "https://overpass-api.de/api/interpreter";

// fake the osmybiz backend with localstorage
export const fakeOsmybizApi = false;
export const osmyBizBackendUrl = "/api/"; //process.env.API_URL;

export const NUM_OF_SECS_TO_SHOW_THE_UNSAVED_CHANGES_NOTIFICATION_DIALOG = 30;
export const NUM_OF_SECS_BEFORE_DELETING_THE_UNSAVED_CHANGES_DATA = 2;

export const MODIFIED = "*";
export const REMOVED = "-";

export const opening_hours_days = [
    {input: "Mo-Tu", value: "Mo/Tu"},
    {input: "Mo-We", value: "Mo/Tu/We"},
    {input: "Mo-Th", value: "Mo/Tu/We/Th"},
    {input: "Mo-Fr", value: "Mo/Tu/We/Th/Fr"},
    {input: "Mo-Sa", value: "Mo/Tu/We/Th/Fr/Sa"},
    {input: "Mo-Su", value: "Mo/Tu/We/Th/Fr/Sa/Su"},
    {input: "Tu-We", value: "Tu/We"},
    {input: "Tu-Th", value: "Tu/We/Th"},
    {input: "Tu-Fr", value: "Tu/We/Th/Fr"},
    {input: "Tu-Sa", value: "Tu/We/Th/Fr/Sa"},
    {input: "Tu-Su", value: "Tu/We/Th/Fr/Sa/Su"},
    {input: "We-Th", value: "We/Th"},
    {input: "We-Fr", value: "We/Th/Fr"},
    {input: "We-Sa", value: "We/Th/Fr/Sa"},
    {input: "We-Su", value: "We/Th/Fr/Sa/Su"},
    {input: "Th-Fr", value: "Th/Fr"},
    {input: "Th-Sa", value: "Th/Fr/Sa"},
    {input: "Th-Su", value: "Th/Fr/Sa/Su"},
    {input: "Fr-Sa", value: "Fr/Sa"},
    {input: "Fr-Su", value: "Fr/Sa/Su"},
    {input: "Sa-Su", value: "Sa/Su"},
]



