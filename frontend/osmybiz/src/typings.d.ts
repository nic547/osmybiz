declare module "*.json" {
  const dataValue: any;
  export default dataValue;
}

declare module "*.png" {
  const value: any;
  export default value;
}

declare module "@vue-leaflet/vue-leaflet";
